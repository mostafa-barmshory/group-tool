#!/bin/bash


if [ -d 'bower_components' ]; then
	cd bower_components
	groups=("angular-material-home" "angular-material-dashboard" "am-wb" "mblowfish" "seen")
	for group in "${groups[@]}"
	do
		for file in `find . -maxdepth 1 -mindepth 1 -type d -name "${group}*"`
		do
			package=`basename $file`
			rm -fR "$file"
			ln -s ~/git/${group}/${package}
			echo "$package		.......................[ok]"
		done
	done
	cd ..
fi

if [ -d 'vendor/pluf' ]; then
	cd ./vendor/pluf
	for file in `find . -maxdepth 1 -mindepth 1 -type d`
	do
		package=`basename $file`
		rm -fR "$file"
		ln -s ~/git/pluf/${package}
		echo "$package		.......................[ok]"
	done
	cd ../../
fi

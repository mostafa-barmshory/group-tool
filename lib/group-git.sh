#!/bin/bash
###########################################################################
#                           License                                       #
#                                                                         #
# FIXME: maso, 2017: adding license file                                                                        #
#                                                                         #
###########################################################################

#
# Commit all changes in groups
# 
# Usage:
#    group commit "this is message"
#
group_git_commit() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git add .
			git commit -m "$1"
		fi
		cd ..
	done
}


#
# Push all projects
# 
# Usage:
#    group push
#
group_git_push() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git push --all
			git push --tags
		fi
		cd ..
	done
}

#
# Pull all projects
# 
# Usage:
#    group pull
#
group_git_pull() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			# git fetch origin
			git checkout master
			git pull origin master
			git checkout develop
			git pull origin develop
			git fetch --tags
		fi
		cd ..
	done
}

#
# Creates a new branch in group
# 
# Usage:
#    group branch <branch-name>
#
group_git_branch() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git branch "$1"
		fi
		cd ..
	done
}

#
# Creates a new tag in group
# 
# Usage:
#    group tag <tag-name> <comment>
#
group_git_tag() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git tag -a "$1" -m "$2"
		fi
		cd ..
	done
}

#
# Deletes a local tag from the all repositories in the group
# 
# Usage:
#    group delete-tag <tag-name>
#
group_git_delete_tag() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git tag -d "$1"
		fi
		cd ..
	done
}

#
# Deletes a remote tag from the all repositories in the group
# 
# Usage:
#    group delete-remote-tag <tag-name>
#
group_git_delete_remote_tag() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git push --delete origin "$1"
		fi
		cd ..
	done
}

#
# Sets a config in all repositories in the group
# 
# Usage:
#    group config <key> <value>
#
group_git_set_config() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git config --add "$1" "$2"
		fi
		cd ..
	done
}

#
# Switchs all repositories to the given branch
# 
# Usage:
#    group config <branch>
#
group_git_switch_branch() {
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git checkout "$1"
		fi
		cd ..
	done
}

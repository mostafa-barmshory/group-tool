#!/bin/bash
###########################################################################
#                           License                                       #
#                                                                         #
# FIXME: maso, 2017: adding license file                                                                        #
#                                                                         #
###########################################################################



group_gitflow_init(){
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ ! -d .git ]; then
			cd ..;
			continue;
		fi
		# maso, 2018: init git flow
		git flow init -fd
		cd ..
	done
}

group_gitflow_release_start(){
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git flow release start $1
		fi
		if [ -f "bower.json" ]; then
			version=$2
 			sed -i -E "s/(\"version\":[[:space:]]*\").+(\")/\"version\":\ \"$1\"/g" "bower.json"
		fi
		if [ -f "package.json" ]; then
			version=$2
 			sed -i -E "s/(\"version\":[[:space:]]*\").+(\")/\"version\":\ \"$1\"/g" "package.json"
		fi
		if [ -f "composer.json" ]; then
			version=$2
 			sed -i -E "s/(\"version\"[[:space:]]*:[[:space:]]*\").+(\")/\"version\"\ :\ \"$1\"/g" "composer.json"
		fi
		cd ..
	done
}

group_gitflow_release(){
	export GIT_MERGE_AUTOEDIT=no;
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git flow release finish -m 'new release of the group' $1
			git checkout develop
		fi
		cd ..
	done
	unset GIT_MERGE_AUTOEDIT;
}

group_gitflow_release_cancel(){
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -d .git ]; then
			git checkout develop
			git branch -D "release/$1"
		fi
		cd ..
	done
}

#!/bin/bash
###########################################################################
#                           License                                       #
#                                                                         #
# FIXME: maso, 2017: adding license file                                                                        #
#                                                                         #
###########################################################################
function group_util_levenshtein {
    if [ "$#" -ne "2" ]; then
        echo "Usage: $0 word1 word2" >&2
    elif [ "${#1}" -lt "${#2}" ]; then
        levenshtein "$2" "$1"
    else
        local str1len=$((${#1}))
        local str2len=$((${#2}))
        local d i j
        for i in $(seq 0 $(((str1len+1)*(str2len+1)))); do
            d[i]=0
        done
        for i in $(seq 0 $((str1len))); do
            d[$((i+0*str1len))]=$i
        done
        for j in $(seq 0 $((str2len))); do
            d[$((0+j*(str1len+1)))]=$j
        done

        for j in $(seq 1 $((str2len))); do
            for i in $(seq 1 $((str1len))); do
                [ "${1:i-1:1}" = "${2:j-1:1}" ] && local cost=0 || local cost=1
                local del=$((d[(i-1)+str1len*j]+1))
                local ins=$((d[i+str1len*(j-1)]+1))
                local alt=$((d[(i-1)+str1len*(j-1)]+cost))
                d[i+str1len*j]=$(echo -e "$del\n$ins\n$alt" | sort -n | head -1)
            done
        done
        echo ${d[str1len+str1len*(str2len)]}
    fi
}

_group_build_line='--------------------------------------------------------'
group_echo_state(){
	echo -ne "\033[2K\r"
	printf "%s %s [%s]" "$1" "${_group_build_line:${#1}}" "$2"
}

group_echo_success(){
	group_echo_state "$1" "OK"
}

group_echo_fail(){
	group_echo_state "$1" "FAIL"
}

group_echo_progress(){
	echo -ne "\033[2K\r"
	printf "%s %s [%s]" "$1" "${_group_build_line:${#1}}" "$2"
}

projects_print_list() {
	echo "Project list"
	echo "==========================================================="
	for project in *;
	do
		if [ ! -d "$project" ]; then
			continue;
		fi
		cd $project
		# Java script project
		if [ -f "bower.json" ]; then
			echo "[Bower   ] ${project}"
		fi
		# PHP project
		if [ -f "composer.json" ]; then
			echo "[Composer] ${project}"
		fi
		cd ..
	done;
	echo "==========================================================="
}

projects_set_version(){
	for folder in ./*; do
		if [ ! -d "$folder" ]; then
			continue;
		fi
		cd "$folder"
		if [ -f "bower.json" ]; then
			version=$2
 			sed -i -E "s/(\"version\"[[:space:]]*:[[:space:]]*\").+(\")/\"version\":\ \"$1\"/g" "bower.json"
		fi
		if [ -f "package.json" ]; then
			version=$2
 			sed -i -E "s/(\"version\"[[:space:]]*:[[:space:]]*\").+(\")/\"version\":\ \"$1\"/g" "package.json"
		fi
		if [ -f "composer.json" ]; then
			version=$2
 			sed -i -E "s/(\"version\"[[:space:]]*:[[:space:]]*\").+(\")/\"version\"\ :\ \"$1\"/g" "composer.json"
		fi
		cd ..
	done
}

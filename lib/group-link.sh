#!/bin/bash
###########################################################################
#                           License                                       #
#                                                                         #
# FIXME: maso, 2017: adding license file                                                                        #
#                                                                         #
###########################################################################


#
# Build a bower based project
# 
#
group_link_project(){
    if [ -d 'bower_components' ]; then
		cd bower_components
		groups=("angular-material-home" "angular-material-dashboard" "am-wb" "mblowfish" "seen")
		for group in "${groups[@]}"
		do
			for file in `find . -maxdepth 1 -mindepth 1 -type d -name "${group}*"`
			do
				package=`basename $file`
				rm -fR "$file"
				ln -s ~/git/${group}/${package}
				echo "$package		.......................[ok]"
			done
		done
		cd ..
	fi
	
	if [ -d 'vendor/pluf' ]; then
		cd ./vendor/pluf
		for file in `find . -maxdepth 1 -mindepth 1 -type d`
		do
			package=`basename $file`
			rm -fR "$file"
			ln -s ~/git/pluf/${package}
			echo "$package		.......................[ok]"
		done
		cd ../..
	fi
}

#
# Build all projects in a group
# 
#
group_link_all(){
	echo "" > ../group-build-log.txt
	for project in *;
	do
		if [ ! -d "$project" ]; then
			continue;
		fi
		cd $project;
		group_link_project;
		cd ..;
	done;
}

#!/bin/bash
###########################################################################
#                           License                                       #
#                                                                         #
# FIXME: maso, 2017: adding license file                                                                        #
#                                                                         #
###########################################################################

_group_build_npm_install(){
	# TODO: maso,2018: check if npm is installed
	npm install >> ../group-build-log.txt 2>&1
}

_group_build_bower_install(){
	# TODO: maso,2018: check if bower is installed
	bower install >> ../group-build-log.txt 2>&1
}

#
# Build a bower based project
# 
#
group_build_bower(){
	PROJECT_NAME=${PWD##*/}
	let progress=10
	group_echo_progress "${PROJECT_NAME}" "${progress}%"
	# run grunt
	while true
	do
	    grunt >> ../group-build-log.txt 2>&1
		state=$?
	    case $state in
	        3) 
	        	# Missing bower module
				group_echo_state "${PROJECT_NAME}" "Install Bower"
	        	_group_build_bower_install
				let progress=progress+10
				group_echo_progress "${PROJECT_NAME}" "${progress}%"
	        	continue
	        	;;
	        99)
	        	# Missing grunt cli
				group_echo_state "${PROJECT_NAME}" "Install NPM"
	        	_group_build_npm_install
				let progress=progress+10
				group_echo_progress "${PROJECT_NAME}" "${progress}%"
	        	continue
	        	;;
	        *) 
	        	break;
	        	;;
	    esac
	done 
	
	
	group_echo_progress "${PROJECT_NAME}" "100%"
	sleep 1
	if [ $state -eq 0 ]; then
		group_echo_success "${PROJECT_NAME}"
	else
		group_echo_fail "${PROJECT_NAME}"
	fi
	echo -ne '\n'
}

group_build_release_bower(){
	PROJECT_NAME=${PWD##*/}
	let progress=10
	group_echo_progress "${PROJECT_NAME}" "${progress}%"
	# run grunt
	while true
	do
	    grunt release >> ../group-build-log.txt 2>&1
		state=$?
	    case $state in
	        3) 
	        	# Missing bower module
				group_echo_state "${PROJECT_NAME}" "Install Bower"
	        	_group_build_bower_install
				let progress=progress+10
				group_echo_progress "${PROJECT_NAME}" "${progress}%"
	        	continue
	        	;;
	        99)
	        	# Missing grunt cli
				group_echo_state "${PROJECT_NAME}" "Install NPM"
	        	_group_build_npm_install
				let progress=progress+10
				group_echo_progress "${PROJECT_NAME}" "${progress}%"
	        	continue
	        	;;
	        *) 
	        	break;
	        	;;
	    esac
	done 
	
	
	group_echo_progress "${PROJECT_NAME}" "100%"
	sleep 1
	if [ $state -eq 0 ]; then
		group_echo_success "${PROJECT_NAME}"
	else
		group_echo_fail "${PROJECT_NAME}"
	fi
	echo -ne '\n'
}

#
# Build a composer based project
# 
#
group_build_composer() {
	# TODO: if composer is not installed?
	if [ ! -f "composer.phar" ]; then
		echo "Installing composer";
		EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
		php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
		ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"
		if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
		then
		    >&2 echo 'ERROR: Invalid installer signature'
		    rm composer-setup.php
		    exit 1
		fi
		
		php composer-setup.php --quiet
		RESULT=$?
		rm composer-setup.php
	fi
	
	# READMORE: https://getcomposer.org/doc/00-intro.md
	if [ -f "run.php" ]; then
		php composer.phar install
		php run.php
	fi
}

group_build_clean_composer(){
	rm -fR ./vendor
	rm -fR ./build
	rm -f ./composer.phar
	rm -f ./composer.lock
	rm -f ./.phpunit.result.cache
	rm -f ./composer-temp.phar
}

#
# Build all projects in a group
# 
#
group_build_all(){
	echo "" > ../group-build-log.txt
	for project in *;
	do
		if [ ! -d "$project" ]; then
			continue;
		fi
		cd $project;
		
		# Java script project
		if [ -f "bower.json" ]; then
			group_build_bower;
		fi
		
		# PHP project
		if [ -f "composer.json" ]; then
			group_build_composer;
		fi
		
		cd ..;
	done;
}

#
# Release project
#
group_build_release_all(){
	echo "" > ../group-build-log.txt
	for project in *;
	do
		if [ ! -d "$project" ]; then
			continue;
		fi
		cd $project;
		
		# Java script project
		if [ -f "bower.json" ]; then
			group_build_release_bower;
		fi
		
		cd ..;
	done;
}

#
# Clean projects
#
group_build_clean(){

	echo "" > ../group-build-log.txt
	for project in *;
	do
		if [ ! -d "$project" ]; then
			continue;
		fi
		cd $project;
		
		# Java script project
		if [ -f "bower.json" ]; then
			group_build_clean_bower;
		fi
		if [ -f "package.json" ]; then
			group_build_clean_npm;
		fi
		if [ -f "composer.json" ]; then
			group_build_clean_composer;
		fi
		
		cd ..;
	done;
}
# Group Tool

A set of tools to work a group of project such as pull, push and release all project in the group.

## How to install

## What a tool?

Following tools are existed here:

- group-init: Initial all project in group.
- group-pull-all: Pull all branches (main and develop) of all project in the group.
- group-push-all: Push all changes (include new tags and updated tags) in all branches (main and develop) of all project.
- group-release-start: Starting new release of all project. 
- group-release: Finishing new release of all project. 

## How to release all project in a group

Note: these tools assume all project in a group are in a same directory on your system.

To publish new release of all project in a group following command should be executed:

    ```
    group-pull-all
    group-release-start <version>
    ```
    
After that you could change projects before finishing release. 
Some works such as change version, build new product and so on could be done before finishing release. 
After doing your desired changes in projects and comminting all changes in projects you should execute follwoing command to finish releasing process.

    ```
    group-release <version>
    group-push-all
    ```
    
Now all projects have new tag named <version>. 
# Manages groups of project

This is a CLI to manage groups of project



## Commands
  build
  test
  release
  
  commit To commit all changes in a group
  push
  pull
  init
  
  help
  
## Git

### Commit

Commits all changes in projects. 

	group commit "This is a new version of system"

### Push

Pushes all commits

	group push

### Branch

Creates a new branch in projects

    group branch <branch-name>
    
### Tag

Creates an annotated tag in the projects

    group tag <tag-name> <comment>
    
Deletes a local tag from all repositories in the group

	group delete-tag <tag-name>
	
Deletes a remote tag from all repositories in the group

	group delete-remote-tag <tag-name>
    
### Config

Sets a key-value in the git configuration of the all repositories

    group set-config <key> <value>
    
For example to set user.email in the all repositories you can use the following command:

    group set-config user.email my.email@example.com
